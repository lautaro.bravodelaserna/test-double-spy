#!/bin/bash

METHOD_NAME=$1
PACKAGE_DIR=$2
APP_NAME=$3
APP_ICON_PATH=$4
NOT_IN_STORE=$5

add() {
    if [ -z "$PACKAGE_DIR" ] 
    then
        echo "Missing package name"        
        exit 1
    fi

    if [ -z "$APP_NAME" ] 
    then
        echo "Missing App Name"        
        exit 1
    fi
    if [ -f "$APP_ICON_PATH" ] 
    then
         ICON_BASE64=$(cat $APP_ICON_PATH | base64)
         ICON_BASE64="data:image/png;base64,$ICON_BASE64"
    fi

    JSON_STRING=$'{"package_dir": "'$PACKAGE_DIR'", "name": "'$APP_NAME'", "icon": "'$ICON_BASE64'", "not_in_store": "'$NOT_IN_STORE'"}'

    curl -d "$JSON_STRING" -H "Content-Type: application/json" -X POST https://platform.etermax.com/release-watcher/apps
}

remove() {
     if [ -z "$PACKAGE_DIR" ] 
    then
        echo "Missing package name"        
        exit 1
    fi
    JSON_STRING=$'{"package_dir": "'$PACKAGE_DIR'", "name": "a" }'
    curl -d "$JSON_STRING" -H "Content-Type: application/json" -X DELETE https://platform.etermax.com/release-watcher/apps
}

case "$1" in
        add)
            add
            ;;
         
        remove)
            remove
            ;;
        *)
            echo $"Usage: $0 {add|remove}"
            exit 1
 
esac