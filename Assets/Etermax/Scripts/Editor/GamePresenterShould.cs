﻿using System;
using System.Collections.Generic;
using System.Linq;
using Etermax.Scripts.TuttiLadders.Services;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Presentation;
using Etermax.Scripts.TuttiTowers.Presentation.Views;
using Moq;
using NUnit.Framework;
using UniRx;
using UnityEngine;

namespace Etermax.Scripts.Editor
{
    public class GamePresenterShould
    {
        private const int TOWER_COUNT = 1;
        private const int VALID_PLAY_RESULT = 3;
        private const int INVALID_PLAY_RESULT = 0;
        private const int FIRST_TOWER_POSITION = 10;
        private const int RESULT_SCORE = 5;
        private const int FIRST_PLAYER_ID = 1;
        private const int SECOND_PLAYER_ID = 2;
        private const int INITIAL_SCORE = 10;
        
        private bool _isPlayStarted;
        private bool _isTurnEnded;
        private bool _isFightStarted;
        private GameData _defaultGameData;
        private Mock<IGameView> _view;
        private SpyGamePresenter _presenter;
        private Mock<IDiceService> _diceService;
        private TurnEndData _turnEndData;


        [SetUp]
        public void SetUp()
        {
            _isPlayStarted = false;
            _isTurnEnded = false;
            _isFightStarted = false;

            
            _defaultGameData = new GameData
            {
                cells = new[]
                {
                    new CellData()
                    {
                        id = 0,
                        positions = new []{ FIRST_TOWER_POSITION , 20},
                        score = 0
                    }
                },
                playerPosition = 0,
                opponentPosition = 0,
                playerId = FIRST_PLAYER_ID,
                boardSize = 25,
                scoreToWin = 1
            };
            _turnEndData = new TurnEndData();
            _view = new Mock<IGameView>();
            _view.Setup(v => v.MovePlayerTowards(It.IsAny<int>(), It.IsAny<int>())).Returns(Observable.Return(0f));
            _diceService = new Mock<IDiceService>();
            _presenter = new SpyGamePresenter(_view.Object, _diceService.Object, OnPlayStarted, OnFightStarted, OnTurnEnded);
        }

        [Test]
        public void Show_Board_On_Game_Start()
        {
            WhenAGameIsStarted();
            ThenBoardIsShown();
        }

        [Test]
        public void Show_Categories_Cells_On_Board()
        {
            WhenAGameIsStarted();
            ThenCategoryCellsAreShown();
        }

        [Test]
        public void Start_First_Play_On_Game_Start()
        {
            WhenAGameIsStarted();
            WhenStartButtonIsPressed();
            ThenPlayIsStarted();
        }

        [Test]
        public void Update_Player_Available_Moves_On_Play_Result()
        {
            GivenAStartedGame();
            WhenFirstPlayIsEnded(VALID_PLAY_RESULT);
            ThenPlayerMovesAreUpdated();
        }

        [Test]
        public void Show_Roll_Dice_Button_On_Available_Moves()
        {
            GivenAStartedGame();
            WhenFirstPlayIsEnded(VALID_PLAY_RESULT);
            ThenRollButtonIsShown();
        }

        [Test]
        public void Hide_Roll_Dice_Button_On_No_Moves()
        {
            GivenAStartedGame();
            WhenFirstPlayIsEnded(1);
            WhenRollButtonIsPressed();
            
            _view.Verify(v => v.HideRollButton(), Times.Exactly(2));
        }

        [Test]
        public void End_Turn_After_Fight_Failed()
        {
            var roll = 1;
            TurnEndData expectedTurnEndData = new TurnEndData()
            {
                currentPlayerPosition = FIRST_TOWER_POSITION,
                towers = new []
                {
                    new CellData()
                    {
                        id = 0,
                        positions = new []{FIRST_TOWER_POSITION, RESULT_SCORE + 1},
                        score = 1
                    }, 
                }
            };
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;
            GivenAStartedGame();
            _presenter.GetFirstTower().SetScore(RESULT_SCORE + 1);

            WhenFirstPlayIsEnded(0);
            WhenDiceRollIsEnded(roll);
            WhenFightIsEndedWithScore(RESULT_SCORE);

            ThenTurnEndDataIs(expectedTurnEndData);
            ThenTurnIsEnded();
        }

        [Test]
        public void Dont_End_Turn_After_Fight_Pass()
        {
            var roll = 1;

            TurnEndData expectedTurnEndData = new TurnEndData()
            {
                currentPlayerPosition = FIRST_TOWER_POSITION,
                towers = new []
                {
                    new CellData()
                    {
                        id = 0,
                        positions = new []{FIRST_TOWER_POSITION, RESULT_SCORE - 2},
                        score = 16
                    },
                }
            };
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;

            GivenAStartedGame();
            _presenter.GetFirstTower().SetScore(RESULT_SCORE);

            WhenFirstPlayIsEnded(0);
            WhenDiceRollIsEnded(roll);
            WhenFightIsEndedWithScore(RESULT_SCORE);

            ThenTurnIsNotEnded();
        }

        [Test]
        public void Roll_Dices_On_Roll_Button_Pressed()
        {
            GivenAStartedGame();
            WhenFirstPlayIsEnded(3);
            WhenRollButtonIsPressed();
            ThenDiceIsRolled();
        }

        [TestCase(0, 4, 4)]
        [TestCase(10, 2, 12)]
        public void Move_Player_On_Dice_Roll_Finished(int startingPosition, int roll, int expectedPosition)
        {
            _defaultGameData.playerPosition = startingPosition;
            GivenAStartedGame();
            WhenDiceRollIsEnded(roll);
            ThenPlayerPositionIsUpdatedAt(expectedPosition);
        }

        [Test]
        public void Update_Player_Positions_On_Game_Start()
        {
            WhenAGameIsStarted();
            ThenPlayerAndOpponentPositionsAreUpdated();
        }

        [Test]
        public void Start_Play_On_Player_Reaching_Tower_Position()
        {
            var roll = 3;
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;
            
            GivenAStartedGame();
            WhenDiceRollIsEnded(roll);
            ThenFightIsStarted();
        }

        [Test]
        public void Set_Score_To_Tower_On_Fight_Ended()
        {
            var roll = 3;
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;
            
            GivenAStartedGame();
            WhenDiceRollIsEnded(roll);
            WhenFightIsEndedWithScore(RESULT_SCORE);
            ThenFirstTowerScoreIs(RESULT_SCORE);
        }

        [Test]
        public void Set_Owner_To_Tower_On_Fight_Ended_With_Higher_Score()
        {
            var roll = 3;
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;
            
            GivenAStartedGame();
            WhenDiceRollIsEnded(roll);
            WhenFightIsEndedWithScore(RESULT_SCORE);
            ThenTowerOwnerIs(FIRST_PLAYER_ID);
        }

        [Test]
        public void Set_Owner_To_Tower_On_Fight_Ended_With_Same_Score()
        {
            var roll = 3;
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;

            GivenAStartedGame();
            _presenter.GetFirstTower().SetScore(RESULT_SCORE);

            WhenDiceRollIsEnded(roll);
            WhenFightIsEndedWithScore(RESULT_SCORE);
            ThenTowerOwnerIs(FIRST_PLAYER_ID);
        }

        [Test]
        public void Dont_Set_Owner_To_Tower_On_Fight_Ended_With_Lower_Score()
        {
            var roll = 3;
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;
            GivenAStartedGame();
            _presenter.GetFirstTower().SetOwnerId(SECOND_PLAYER_ID);
            _presenter.GetFirstTower().SetScore(10);

            WhenDiceRollIsEnded(roll);
            WhenFightIsEndedWithScore(RESULT_SCORE);
            ThenTowerOwnerIs(SECOND_PLAYER_ID);
        }

        [Test]
        public void Reduce_Score_To_Tower_On_Fight_Ended_With_Lower_Score()
        {
            var roll = 3;
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;
            GivenAStartedGame();
            _presenter.GetFirstTower().SetOwnerId(SECOND_PLAYER_ID);
            _presenter.GetFirstTower().SetScore(INITIAL_SCORE);

            WhenDiceRollIsEnded(roll);
            WhenFightIsEndedWithScore(RESULT_SCORE);
            ThenFirstTowerScoreIs(INITIAL_SCORE-RESULT_SCORE);
            ThenTowerOwnerIs(SECOND_PLAYER_ID);
        }
        
        [Test]
        public void Add_Roll_To_Player_When_Moving_To_Own_Tower()
        {
            var roll = 3;
            var currentRolls = _presenter.GetAvailableRolls();
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;

            GivenAStartedGame();
            _presenter.GetFirstTower().SetOwnerId(FIRST_PLAYER_ID);
            _presenter.GetFirstTower().SetScore(INITIAL_SCORE);
            WhenDiceRollIsEnded(roll);
            ThenFightIsNotStarted();
            ThenPlayerMovesAreUpdated();
            ThenAvailableRollsIs(currentRolls + 1);
        }

        [Test]
        public void Show_Win_Popup_When_Win_Condition_Met()
        {
            var roll = 3;
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;
            //_defaultGameData.towers[0].SetOwnerId(FIRST_PLAYER_ID);

            GivenAStartedGame();
            WhenDiceRollIsEnded(roll);
            WhenFightIsEndedWithScore(RESULT_SCORE);

            _view.Verify(v => v.ShowWinPopup(), Times.Once);
            Assert.IsFalse(_isTurnEnded, "Turn is Ended");
        }

        [TestCase(24, 1, 0)]
        [TestCase(24, 5, 4)]
        public void Reposition_Player_When_Finish_The_Board(int startPosition, int roll, int finalPosition)
        {
            _defaultGameData.playerPosition = startPosition;
            
            GivenAStartedGame();
            WhenDiceRollIsEnded(roll);
            ThenPlayerFinalPositionIs(finalPosition);
        }

        [Test]
        public void Reduce_Roll_Count_On_Dice_Roll()
        {
            var availableRolls = 3;
            GivenAStartedGame();
            WhenFirstPlayIsEnded(availableRolls);
            WhenRollButtonIsPressed();
            ThenRollCountIs(availableRolls);
        }

        [Test]
        public void Hide_Roll_Button_On_Start()
        {
            WhenAGameIsStarted();
            ThenRollButtonIsHidden();
        }

        [Test]
        public void Show_Roll_Button_When_Additional_Roll_Is_Gained_After_Using_Last_Roll()
        {
            var roll = 3;
            _defaultGameData.playerPosition = FIRST_TOWER_POSITION - roll;
            GivenAStartedGame();
            _presenter.GetFirstTower().SetOwnerId(FIRST_PLAYER_ID);
            _presenter.GetFirstTower().SetScore(INITIAL_SCORE);

            WhenFirstPlayIsEnded(1);
            WhenRollButtonIsPressed();
            WhenDiceRollIsEnded(roll);
            ThenFightIsNotStarted();
            
            _view.Verify( v => v.ShowRollButton(), Times.AtLeast(2));
        }

        [Test]
        public void Show_Start_Button_On_Start()
        {
            WhenAGameIsStarted();
            
            _view.Verify(v => v.ShowStartButton(), Times.Once);
            _view.Verify(v => v.ShowRollButton(), Times.Never);
        }

        [Test]
        public void Hide_Start_Button_On_Play_Ended()
        {
            GivenAStartedGame();
            WhenFirstPlayIsEnded(3);
            _view.Verify(v => v.HideStartButton(), Times.Once);
        }

        [Test]
        public void End_Turn_When_No_More_Moves_Are_Available()
        {
            GivenAStartedGame();
            WhenFirstPlayIsEnded(1);
            WhenRollButtonIsPressed();
            ThenAvailableRollsIs(0);
            ThenTurnIsEnded();
        }

        [Test]
        public void Enter_To_Insane_Mode_On_Roll_A_Six()
        {
            GivenAStartedGame();
            WhenFirstPlayIsEnded(1);
            _presenter.WhenPlayerEndedMoving(6);
    
            WhenRollButtonIsPressed();
            WhenRollButtonIsPressed();
            WhenRollButtonIsPressed();
            
        }

        private void GivenAStartedGame()
        {
            WhenAGameIsStarted();
        }

        private void WhenFightIsEndedWithScore(int resultScore)
        {
            _presenter.OnFightEnded(resultScore);
        }

        private void WhenDiceRollIsEnded(int roll)
        {
            //_presenter.OnDiceRollEnded(roll);
            _presenter.WhenPlayerEndedMoving(roll);
        }

        private void WhenRollButtonIsPressed()
        {
            _presenter.RollButtonPressed();
        }

        private void WhenFirstPlayIsEnded(int playResult)
        {
            _presenter.OnPlayEnded(playResult);
        }

        private void WhenAGameIsStarted()
        {
            _presenter.StartGame(_defaultGameData);
        }

        private void WhenStartButtonIsPressed()
        {
            _presenter.OnStartButtonPressed();
        }

        private void ThenBoardIsShown()
        {
            _view.Verify(v => v.ShowBoard(), Times.Once);
        }

        private void ThenCategoryCellsAreShown()
        {
            _view.Verify(v => v.ShowCategoryCellsAt(It.IsAny<int>(), It.IsAny<CategoryCell>()), Times.Exactly(2));
        }

        private void ThenPlayIsStarted()
        {
            Assert.IsTrue(_isPlayStarted, "Play Is Started");
        }

        private void ThenPlayerMovesAreUpdated()
        {
            _view.Verify(v => v.UpdatePlayerAvailableDiceRolls(It.IsAny<int>()), Times.Once);
        }

        private void ThenFirstPlayIsNotStarted()
        {
            Assert.IsFalse(_isPlayStarted, "Play Is Started");
        }

        private void ThenRollButtonIsShown()
        {
            _view.Verify(v => v.ShowRollButton(), Times.Once);
        }

        private void ThenRollButtonIsHidden()
        {
            _view.Verify(v => v.ShowRollButton(), Times.Never);
            _view.Verify(v => v.HideRollButton(), Times.Once);
        }

        private void ThenTurnIsEnded()
        {
            Assert.IsTrue(_isTurnEnded, "Turn was ended");
        }

        private void ThenTurnIsNotEnded()
        {
            Assert.IsFalse(_isTurnEnded, "Turn was ended");
        }

        private void ThenDiceIsRolled()
        {
            _diceService.Verify(ds => ds.Roll(), Times.Once());
        }

        private void ThenPlayerPositionIsUpdatedAt(int expectedPosition)
        {
            //_view.Verify(v => v.SetPlayerPosition(expectedPosition), Times.Once);
            Assert.AreEqual(expectedPosition, _presenter.GetPlayerPosition());
        }

        private void ThenPlayerAndOpponentPositionsAreUpdated()
        {
            _view.Verify(v => v.SetPlayerPosition(It.IsAny<int>()), Times.Once);
            _view.Verify(v => v.SetOpponentPosition(It.IsAny<int>()), Times.Once);
        }

        private void ThenFirstTowerScoreIs(int resultScore)
        {
            Assert.AreEqual(resultScore, _presenter.GetFirstTower().GetScore(), "Tower score");
        }

        private void ThenFightIsStarted()
        {
            Assert.IsTrue(_isFightStarted, "Fight Is Started");
        }

        private void ThenFightIsNotStarted()
        {
            Assert.IsFalse(_isFightStarted, "Fight Is Started");
        }

        private void ThenTowerOwnerIs(int playerID)
        {
            Assert.AreEqual(playerID, _presenter.GetFirstTower().GetOwnerId());
        }

        private void ThenAvailableRollsIs(int availableRolls)
        {
            Assert.AreEqual(availableRolls, _presenter.GetAvailableRolls());
        }

        private void ThenPlayerFinalPositionIs(int finalPosition)
        {
            Assert.AreEqual(finalPosition, _presenter.GetPlayerPosition());
        }

        private void ThenTurnEndDataIs(TurnEndData expectedTurnEndData)
        {
            Assert.AreEqual(expectedTurnEndData, _turnEndData);
        }

        private void OnPlayStarted()
        {
            _isPlayStarted = true;
        }

        private void OnTurnEnded(TurnEndData data)
        {
            _isTurnEnded = true;
            _turnEndData = data;
        }

        private void OnFightStarted(int scoreToBeat)
        {
            _isFightStarted = true;
        }

        private void ThenRollCountIs(int availableRolls)
        {
            Assert.AreEqual(availableRolls - 1, _presenter.GetAvailableRolls());
            _view.Verify(v => v.UpdatePlayerAvailableDiceRolls(availableRolls - 1), Times.Once);
        }
    }
    
    public class SpyGamePresenter : GamePresenter
    {
        public SpyGamePresenter(IGameView view, IDiceService diceService, Action onPlayStarted, Action<int> onFightStarted, Action<TurnEndData>
            onTurnEnded) : base(view, diceService, onPlayStarted, onFightStarted, onTurnEnded)
        {
            
        }

        public CategoryCell GetFirstTower()
        {
            return _categoryCells[0];
        }

        public int GetAvailableRolls()
        {
            return availableRolls;
        }
        public int GetAvailableRollsWith(int amount)
        {
            return amount;
        }

        public int GetPlayerPosition()
        {
            return currentPlayerPosition;
        }

        public void WhenPlayerEndedMoving(int roll)
        {
            OnPlayerEndedMoving(roll);
        }
    }

    public class StubGamePresenter : GamePresenter
    {
        public StubGamePresenter(IGameView view, IDiceService diceService, Action onPlayStarted, Action<int> onFightStarted, Action<TurnEndData>
        onTurnEnded) : base(view, diceService, onPlayStarted, onFightStarted, onTurnEnded)
        {
            
        }

        public CategoryCell GetFirstTower()
        {
            return _categoryCells[0];
        }

        public int GetAvailableRolls()
        {
            return availableRolls;
        }
        public int GetAvailableRollsWith(int amount)
        {
            return amount;
        }

        public int GetPlayerPosition()
        {
            return currentPlayerPosition;
        }

        public void WhenPlayerEndedMoving(int roll)
        {
            OnPlayerEndedMoving(roll);
        }
    }
}
