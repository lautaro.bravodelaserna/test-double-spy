using System.Linq;
using Etermax.Scripts.Services.Random;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Services;
using Moq;
using NUnit.Framework;
using UnityEngine;

namespace Etermax.Scripts.Editor
{
    public class ServerShould
    {
        private CategoryDataService _categoryDataService;
        private Mock<IRandomService> _randomService;
        private Server _server;
        
        [SetUp]
        public void SetUp()
        {
            _randomService = new Mock<IRandomService>();
            var gameData = Resources.Load<ParsedData>("GameData");
            _categoryDataService = new CategoryDataService(gameData.WordItems);
            _server = new Server(_randomService.Object, _categoryDataService);
        }

        [Ignore("This test doesnt't make sense")]
        [Test]
        public void Returns_Game_Data()
        {
            var expectedGameData = new GameData
            {
                cells = new[]
                {
                    new CellData {id = 0, positions = new[] {5}, score = 0},
                    new CellData {id = 0, positions = new[] {10}, score = 0}
                },
                playerPosition = 0,
                opponentPosition = 0,
                playerId = 1,
                scoreToWin = 3
            };

            var result = _server.GetGameData();

            Assert.AreEqual(expectedGameData, result);
        }

        [Test]
        public void Returns_Updated_Data_After_Turn_End()
        {
            var firstData = _server.GetGameData();

            var turnEndData = new TurnEndData
            {
                currentPlayerPosition = 7,
                towers = new[]
                {
                    new CellData {id = 1, positions = new[] {5}, score = 69},
                    new CellData {id = 0, positions = new[] {10}, score = 0}
                },
            };
            var expectedGameData = new GameData
            {
                cells = new[]
                {
                    new CellData {id = 1, positions = new[] {5}, score = 69},
                    new CellData {id = 0, positions = new[] {10}, score = 0}
                },
                playerPosition = firstData.opponentPosition,
                opponentPosition = turnEndData.currentPlayerPosition,
                playerId = 2,
                scoreToWin = 8
            };

            _server.OnTurnEnded( turnEndData);
            
            var result = _server.GetGameData();
            Assert.AreEqual(expectedGameData, result);

        }
    }
}