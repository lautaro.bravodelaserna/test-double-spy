using System;
using System.Collections.Generic;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Presentation;
using Etermax.Scripts.TuttiTowers.Presentation.Views;
using Etermax.Scripts.TuttiTowers.Services;
using Moq;
using NUnit.Framework;
using UnityEngine;

namespace Etermax.Scripts.Editor
{
    public class TowerMiniGameShould
    {
        private Mock<ITowerMiniGameValidatorService> _service;
        private Mock<ITowerMiniGameView> _view;
        private SpyTowerMiniGamePresenter _presenter;
        private int _resultScore;

        private readonly TowerMiniGameData _defaultData = new TowerMiniGameData()
        {
            Category = "ANIMALS"
        };


        [SetUp]
        public void SetUp()
        {
            _resultScore = 0;
            _service = new Mock<ITowerMiniGameValidatorService>();
            _view = new Mock<ITowerMiniGameView>();
            _presenter = new SpyTowerMiniGamePresenter(_view.Object, _service.Object, OnMiniGameEnded);
        }

        [Test]
        public void Show_Category_On_Start()
        {
            WhenGameInits();
            ThenCategoryIsShown();
        }

        [Test]
        public void Show_Timer_On_Start()
        {
            WhenGameInits();
            ThenTimerIsStarted();
        }

        [Test]
        public void Show_Word_On_User_Input()
        {
            GivenAStartedGame();
            WhenUserInputs("PUERCO");
            ThenWordIsShownOnce();
        }

        [Test]
        public void Show_Word_Once_On_Duplicated_Input()
        {
            GivenAStartedGame();
            WhenUserInputs("PUERCO");
            WhenUserInputs("PUERCO");
            ThenWordIsShownOnce();
        }

        [Test]
        public void Show_Valid_Feedback_On_Valid_Input()
        {
            _service.Setup(s => s.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            GivenAStartedGame();
            WhenUserInputs("PUERCO");
            ThenValidFeedbackIsShown();
        }

        [Test]
        public void Show_Invalid_Feedback_On_Duplicated_Input()
        {
            _service.Setup(s => s.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            GivenAStartedGame();
            WhenUserInputs("PUERCO");
            WhenUserInputs("PUERCO");
            ThenInvalidFeedbackIsShown();
        }

        [Test]
        public void Show_Invalid_Feedback_On_Invalid_Input()
        {
            _service.Setup(s => s.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns(false);
            GivenAStartedGame();
            WhenUserInputs("PELOTA");
            ThenInvalidFeedbackIsShown();
        }

        [Test]
        public void Show_Score_Popup_On_Timeout()
        {
            _service.Setup(s => s.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            GivenAStartedGame();
            WhenUserInputs("PUERCO");
            WhenGameTimeouts();
            ThenResultScreenIsShownWithValidScore();
        }

        [Test]
        public void Send_Event_On_Score_Popup_Dismissed()
        {
            _service.Setup(s => s.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            GivenAStartedGame();
            WhenUserInputs("PUERCO");
            WhenGameTimeouts();
            WhenPopupIsDismissed();
            ThenResultScoreIs(1);
        }

        [Test]
        public void Increase_Answers_Count_When_Submit_Input()
        {
            GivenAStartedGame();
            WhenSubmittedThreeAnswersWith("PUERCO");
            ThenCountOfAnswersSubmittedAre(3);
        }

        private void GivenAStartedGame()
        {
            _presenter.Init(_defaultData, 5);
        }

        private void WhenPopupIsDismissed()
        {
            _presenter.OnScorePopupDismissed();
        }

        private void WhenGameTimeouts()
        {
            _presenter.OnTimeOut();
        }

        private void WhenUserInputs(string wordInput)
        {
            _presenter.OnWordInput(wordInput);
        }

        private void WhenGameInits()
        {
            _presenter.Init(_defaultData, 5);
        }
        
        private void WhenSubmittedThreeAnswersWith(string answer)
        {
            _presenter.SubmitAnswer(answer);
            _presenter.SubmitAnswer(answer);
            _presenter.SubmitAnswer(answer);
        }

        private void ThenTimerIsStarted()
        {
            _view.Verify(v => v.ShowTimer(It.IsAny<float>()), Times.Once);
        }

        private void ThenCategoryIsShown()
        {
            _view.Verify(v => v.ShowCategory(It.IsAny<string>()), Times.Once);
        }

        private void ThenWordIsShownOnce()
        {
            _view.Verify(v => v.ShowWordInput(It.IsAny<string>()), Times.Once);
        }

        private void ThenValidFeedbackIsShown()
        {
            _view.Verify(v => v.ShowValidFeedback(), Times.Once);
        }

        private void ThenInvalidFeedbackIsShown()
        {
            _view.Verify(v => v.ShowInvalidFeedback(), Times.Once);
        }

        private void ThenResultScreenIsShownWithValidScore()
        {
            _view.Verify(v => v.ShowResultScreen(1));
        }

        private void ThenResultScoreIs(int score)
        {
            Assert.AreEqual(score, _resultScore);
        }
        
        private void ThenCountOfAnswersSubmittedAre(int count)
        {
            Assert.AreEqual(count, _presenter.GetCountOfAnswersSubmitted());
        }

        private void OnMiniGameEnded(int result)
        {
            _resultScore = result;
        }
        
        
    }
}

public class SpyTowerMiniGamePresenter : TowerMiniGamePresenter
{

    private int _answersSubmitted;
    
    
    public SpyTowerMiniGamePresenter(ITowerMiniGameView view, ITowerMiniGameValidatorService service, Action<int> onMiniGameEnded) : base(view, service, onMiniGameEnded)
    {
        
    }

    public void SubmitAnswer(string answer)
    {
        _answersSubmitted++;
        OnWordInput(answer);
    }

    public int GetCountOfAnswersSubmitted()
    {
        return _answersSubmitted;
    }
    

  
}