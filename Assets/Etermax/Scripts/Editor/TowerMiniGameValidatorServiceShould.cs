using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Services;
using NUnit.Framework;
using UnityEngine;

namespace Etermax.Scripts.Editor
{
    public class TowerMiniGameValidatorServiceShould
    {

        private TowerMiniGameValidatorService _service;
        
        [SetUp]
        public void SetUp()
        {
            var gameData = Resources.Load<ParsedData>("GameData");
            _service = new TowerMiniGameValidatorService(new CategoryDataService(gameData.WordItems));
        }

        [Test]
        public void Return_True_On_Valid_Input()
        {
            var response = _service.Validate("LIBROS", "ANNA KARENINA");
            Assert.IsTrue(response);
        }


        [Test]
        public void Return_False_On_Invalid_Input()
        {
             var response = _service.Validate("LIBROS", "ANNA KARENGINA");
             Assert.IsFalse(response);
        }
    }
}