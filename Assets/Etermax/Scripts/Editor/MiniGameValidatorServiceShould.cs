using System.Collections.Generic;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Services;
using Moq;
using NUnit.Framework;
using UnityEngine;

namespace Etermax.Scripts.Editor
{
    public class MiniGameValidatorServiceShould
    {
        private const string STARTING_LETTER = "A";
        private string[] VALID_USER_INPUT;
        private string[] INVALID_USER_INPUT;
        private string[] CATEGORIES;
        private MiniGameValidatorService _service;
        private Mock<ICategoryDataService> _categoryDataService;
        
        [SetUp]
        public void SetUp()
        {
            CATEGORIES = new []
            {
                "ANIMAL"
            };
            VALID_USER_INPUT = new[] { "ABEJA"};
            INVALID_USER_INPUT = new[] {"BABEJA"};
            var gameData = Resources.Load<ParsedData>("GameData");
            _categoryDataService = new Mock<ICategoryDataService>();
            _service = new MiniGameValidatorService(_categoryDataService.Object);
            _categoryDataService.Setup(c => c.GetWordsByCategory()).Returns(new Dictionary<string, List<string>>()
            {
                { "ANIMAL", new List<string> {"ABEJA"} }
            });

        }

        [Test]
        public void Return_True_On_Valid_Word()
        {
            var validations = _service.Validate(STARTING_LETTER, VALID_USER_INPUT, CATEGORIES);
            Assert.IsTrue(validations[0]);
        }

        [Test]
        public void Return_False_On_Invalid_Word()
        {
            var validations = _service.Validate(STARTING_LETTER, INVALID_USER_INPUT, CATEGORIES);
            Assert.IsFalse(validations[0]);
        }
    }
}