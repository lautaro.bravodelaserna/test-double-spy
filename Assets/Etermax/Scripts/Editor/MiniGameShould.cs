﻿
using System;
using System.Collections.Generic;
using Etermax.Scripts.TuttiLadders.Services;
using Etermax.Scripts.TuttiTowers.Presentation;
using Etermax.Scripts.TuttiTowers.Presentation.Views;
using Etermax.Scripts.TuttiTowers.Services;
using Moq;
using NUnit.Framework;

namespace Etermax.Scripts.Editor
{
    public class MiniGameShould
    {
        private StubMinigamePresenter _presenter;
        private Mock<IMiniGameView> _view;
        private Mock<IMiniGameValidatorService> _validatorService;
        private MiniGameData _defaultData;

        private int _resultAnswers;
        
        [SetUp]
        public void Setup()
        {
            _view = new Mock<IMiniGameView>();
            _validatorService = new Mock<IMiniGameValidatorService>();
            _presenter = new StubMinigamePresenter(_view.Object, _validatorService.Object, i => _resultAnswers = i);
            _defaultData = new MiniGameData
            {
                categories = new[]
                {
                    "ANIMALES",
                    "COLORES",
                    "PAISES"
                },
                character = "P",
                maxTime = 30
            };
            
            _resultAnswers = 0;
        }
        
        [Test]
        public void Show_Starting_Letter_On_Start()
        {
            WhenMiniGameStarts();
            ThenCharacterIsShown();
        }

        [Test]
        public void Show_All_Categories_On_Start()
        {
            WhenMiniGameStarts();
            ThenCategoriesIsShown();
        }

        [Test]
        public void Show_User_Response_On_Category()
        {
            int categoryIndex = 1;
            string response = "Perro";
            
            WhenMiniGameStarts();
            WhenCategoryIsSelected(categoryIndex);
            WhenUserResponse(response);
            ThenUserResponseIsShown();
        }
        

        [Test]
        public void Show_Timer_End_On_End_Time()
        {
            WhenMiniGameStarts();
            WhenTimeFinishes();
            ThenShowEndTimeFeedback();
        }

        [Test]
        public void Show_Correct_Answers()
        {
            WhenMiniGameStarts();
            WhenTimeFinishes();
            ThenShowAnswersFeedback();
            ThenAnswersAreValidated();
        }

        [Test]
        public void Return_Answers_Count_On_End_Plus_One()
        {
            _validatorService.Setup(v => v.Validate( It.IsAny<string>(), It.IsAny<string[]>(),It.IsAny<string[]>()))
                .Returns(new[]
                {
                    true,
                    true,
                    false,
                    false,
                    true
                });
            
            WhenMiniGameStarts();
            WhenTimeFinishes();
            WhenMiniGameEnded();
            ThenExpectedScoreIs(3 + 1);
        }

        [Test]
        public void On_Selected_Category_Show_Selection_Feedback()
        {
            GivenMiniGameStarted();
            WhenCategoryIsSelected(1);
            ThenSelectionFeedbackIsShown();
        }

        [Test]
        public void On_Selected_Category_Hide_Previous_Selection_Feedback()
        {
            var firstCategoryIndexSelected = 1;
            var secondCategorySelected = 2;
            GivenMiniGameStarted();
            WhenCategoryIsSelected(firstCategoryIndexSelected);
            WhenCategoryIsSelected(secondCategorySelected);
            ThenSelectionFeedbackIsHidden(firstCategoryIndexSelected); 
        }

        [Test]
        public void On_Submit_Input_For_Category_Select_Next_Category()
        {
            GivenMiniGameStarted();
            WhenCategoryIsSelected(0);
            WhenUserResponse("PERRO");
            ThenSelectionFeedbackIsShownOn(1);
        }

        [TestCase(0,2,1)]
        [TestCase(1,2,0)]
        public void On_Submit_Input_For_Last_Category_Select_First_Empty_Category(int firstIndex, int secondIndex, int expectedSelectionIndex)
        {
            GivenMiniGameStarted();
            WhenUserInputOn(firstIndex, "PERRO");
            WhenUserInputOn(secondIndex, "PERRO");
            ThenSelectionFeedbackIsShownOn(expectedSelectionIndex);
        }

        private void WhenUserInputOn(int selectedCategory, string response)
        {
            WhenCategoryIsSelected(selectedCategory);
            WhenUserResponse(response);
        }

        private void GivenMiniGameStarted()
        {
            WhenMiniGameStarts();
        }

        private void WhenCategoryIsSelected(int firstCategoryIndexSelected)
        {
            _presenter.OnCategorySelected(firstCategoryIndexSelected);
        }

        private void WhenMiniGameEnded()
        {
            _presenter.OnMiniGameEnded();
        }

        private void WhenMiniGameStarts()
        {
            _presenter.Init(_defaultData);
        }

        private void WhenUserResponse(string response)
        {
            _presenter.OnSubmit(response);
        }

        private void WhenTimeFinishes()
        {
            _presenter.OnFinishTimer();
        }

        private void ThenCharacterIsShown()
        {
            _view.Verify(v => v.ShowCharacter(It.IsAny<string>()), Times.Once);
        }

        private void ThenCategoriesIsShown()
        {
            _view.Verify(v => v.ShowAllCategories(It.IsAny<string[]>()), Times.Once);
        }

        private void ThenUserResponseIsShown()
        {
            _view.Verify(v => v.ShowUserResponseOnCategory(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        private void ThenShowEndTimeFeedback()
        {
            _view.Verify(v => v.ShowTimerEndFeedback(It.IsAny<int>()), Times.Once);
        }

        private void ThenShowAnswersFeedback()
        {
            _view.Verify(v => v.ShowAnswersFeedback(It.IsAny<bool[]>()), Times.Once);
        }

        private void ThenAnswersAreValidated()
        {
            _validatorService.Verify(v => v.Validate(It.IsAny<string>(), It.IsAny<string[]>(),It.IsAny<string[]>())
                , Times.Once);
        }

        private void ThenExpectedScoreIs(int expected)
        {
            Assert.AreEqual(expected, _resultAnswers);
        }

        private void ThenSelectionFeedbackIsShown()
        {
            _view.Verify(v => v.ShowSelectionFeedbackOn(It.IsAny<int>()), Times.AtLeastOnce);
        }

        private void ThenSelectionFeedbackIsHidden(int firstCategoryIndexSelected)
        {
            _view.Verify(v => v.HideSelectionFeedbackOn(firstCategoryIndexSelected), Times.Once);
        }

        private void ThenSelectionFeedbackIsShownOn(int categoryIndex)
        {
            _view.Verify(v => v.ShowSelectionFeedbackOn(categoryIndex), Times.AtLeast(1));
        }


        private class StubMinigamePresenter : MiniGamePresenter
        {
            public StubMinigamePresenter(IMiniGameView view, IMiniGameValidatorService validatorService, Action<int> onMiniGameEnded) : base(view, validatorService, onMiniGameEnded)
            {

            }

            protected override void ExecuteTimer(float seconds, Action action)
            {
                action();
            }
        }
    }
}
