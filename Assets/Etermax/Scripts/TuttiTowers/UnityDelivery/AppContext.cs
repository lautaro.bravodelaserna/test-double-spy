using System;
using Etermax.Scripts.ProteoTemplate.UnityDelivery.Views;
using Etermax.Scripts.Services.Random;
using Etermax.Scripts.TuttiLadders.Services;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Services;
using UniRx;
using UnityEngine;

namespace Etermax.Scripts.TuttiTowers.UnityDelivery
{
    public class AppContext : MonoBehaviour
    {
        [SerializeField] private MainMenuView menuView;
        [SerializeField] private GameView gameView;
        [SerializeField] private MiniGameView miniGameView;
        [SerializeField] private AcceptPopupView turnPopup;
        [SerializeField] private TowerMiniGameView towerMiniGameView;

        private IDiceService _diceService;
        private UnityRandomService _randomService;
        private IMiniGameValidatorService _miniGameService;
        private ITowerMiniGameValidatorService _towerMiniGameValidatorService;
        private CategoryDataService _categoryDataService;
        private Server _server;
        private void Start()
        {
            _randomService = new UnityRandomService();
            _diceService = new DiceService(_randomService);
            
            var gameData = Resources.Load<ParsedData>("GameData");
            _categoryDataService = new CategoryDataService(gameData.WordItems);
            _miniGameService = new MiniGameValidatorService(_categoryDataService);
            _server = new Server(_randomService, _categoryDataService);
            _towerMiniGameValidatorService = new TowerMiniGameValidatorService(_categoryDataService);
            
            gameView.gameObject.SetActive(false);
            menuView.gameObject.SetActive(true);
            miniGameView.gameObject.SetActive(false);

            gameView.Init(_diceService, OnPlayStarted, OnFightStarted, OnTurnEnded );
            menuView.Init(OnGameStart);
            miniGameView.Init(_miniGameService, OnMiniGameEnded);
            towerMiniGameView.Init(_towerMiniGameValidatorService, OnTowerMiniGameEnded);
            turnPopup.Init(OnAcceptTurnEnd, "Ok", "Out of moves", "Your turn is ended");
        }

        private void OnAcceptTurnEnd()
        {
            turnPopup.Hide();
            //_server.OnTurnEnd();
            gameView.StartGame(_server.GetGameData());
        }

        private void OnGameStart()
        {
            gameView.gameObject.SetActive(true);
            menuView.gameObject.SetActive(false);
            
            gameView.StartGame(_server.GetGameData());
        }

        private void OnPlayStarted()
        {
            //miniGameView.gameObject.SetActive(true);
            //miniGameView.StartMiniGame(_server.GetNewMiniGameData());
            OnMiniGameEnded(3);
        }

        private void OnMiniGameEnded(int rolls)
        {
            miniGameView.gameObject.SetActive(false);

            gameView.OnPlayEnded(rolls);
        }

        private void OnFightStarted(int scoreToBeat)
        {
            //Debug.Log("Show Fight");
            Observable.Timer(TimeSpan.FromSeconds(2))
                .Do(_ =>
                {
                    towerMiniGameView.gameObject.SetActive(true);
                    towerMiniGameView.StartMiniGame(_server.GetNewTowerMiniGameData(), scoreToBeat);
                })
                .Subscribe();
        }

        private void OnTowerMiniGameEnded(int score)
        {
            towerMiniGameView.gameObject.SetActive(false);
            gameView.OnFightEnded(score);
        }

        private void OnTurnEnded(TurnEndData data)
        {
            Debug.Log("Show Turn Ended Popup");
            _server.OnTurnEnded(data);
            turnPopup.Show();
        }
    }
}