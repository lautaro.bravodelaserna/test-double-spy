using System;
using Etermax.Scripts.ProteoTemplate.UnityDelivery.Views;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Presentation;
using Etermax.Scripts.TuttiTowers.Presentation.Views;
using Etermax.Scripts.TuttiTowers.Services;
using TMPro;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace Etermax.Scripts.TuttiTowers.UnityDelivery
{
    public class TowerMiniGameView : MonoBehaviour, ITowerMiniGameView
    {
        [SerializeField] private TextMeshProUGUI categoryText;
        [SerializeField] private TimerView timerView;
        [SerializeField] private TextMeshProUGUI[] inputTexts;
        [SerializeField] private AcceptPopupView popup;
        [SerializeField] private KeyboardView keyboardView;
        [SerializeField] private TextMeshProUGUI healthCounter;
        
        private TowerMiniGamePresenter _presenter;
        private IDisposable _keyboardDisposable;

        public void Init(ITowerMiniGameValidatorService service, Action<int> onMiniGameEnded)
        {
            _presenter = new TowerMiniGamePresenter(this, service, onMiniGameEnded);
            
            keyboardView.Init(OnKeyboardSubmit);
            keyboardView.DisableSubmitButtonAutoToggle();
            keyboardView.ToggleInputContainer(false);
            
            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(KeyCode.F))
                .Do(_ => _presenter.Cheat())
                .Do(_ => _presenter.OnTimeOut())
                .Subscribe();
        }

        public void StartMiniGame(TowerMiniGameData data, int scoreToBeat)
        {

            ResetInputTexts();
            
            if (_keyboardDisposable != null) _keyboardDisposable.Dispose();
            _presenter.Init(data, scoreToBeat);
            _keyboardDisposable = keyboardView.GetCurrentInputObservable().Do(OnKeyboardInputUpdate).Subscribe();
            keyboardView.Show(25, "");
        }

        public void SetHealth(int healthPoints)
        {
            healthCounter.text = healthPoints.ToString();
        }

        public void ShowCategory(string categoryName)
        {
            categoryText.text = categoryName;
        }

        public void ShowTimer(float maxTime)
        {
            timerView.StartTimer((int)maxTime, _presenter.OnTimeOut, _ => { }, 5);
        }

        public void ShowWordInput(string wordInput)
        {
            for (int i = 0; i < inputTexts.Length - 1 ; i++)
            {
                inputTexts[i].text = inputTexts[i + 1].text;
                inputTexts[i].color = inputTexts[i + 1].color;
            }
            inputTexts[inputTexts.Length - 1].text = String.Empty;
        }

        public void ShowValidFeedback()
        {
            Debug.Log("Bien!");
            inputTexts[inputTexts.Length - 2].color = Color.green;
        }

        public void ShowInvalidFeedback()
        {
            Debug.Log("<b><color=Red>Mal!</color></b>");
            inputTexts[inputTexts.Length - 2].color = Color.red;
        }

        public void ShowResultScreen(int score)
        {
            popup.Init(OnPopupButtonContinue, "Continuar", "Asedio Finalizado", "Hiciste " + score + " puntos");
            popup.Show();
        }

        private void OnPopupButtonContinue()
        {
            popup.Hide();
            _presenter.OnScorePopupDismissed();
            keyboardView.Hide();
        }

        private void OnKeyboardSubmit(string input)
        {
            _presenter.OnWordInput(input);
            keyboardView.Show(25, "");
        }

        private void OnKeyboardInputUpdate(string inputString)
        {
            inputTexts[inputTexts.Length - 1].text = inputString;
        }

        private void ResetInputTexts()
        {
            foreach (TextMeshProUGUI inputText in inputTexts)
            {
                inputText.text = string.Empty;
                inputText.color = Color.white;
            }
        }
    }
}