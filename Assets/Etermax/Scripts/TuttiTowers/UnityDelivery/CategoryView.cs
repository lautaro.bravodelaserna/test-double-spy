using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Etermax.Scripts.TuttiTowers.UnityDelivery
{
    public class CategoryView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI categoryText;
        [SerializeField] private TextMeshProUGUI userResponseText;
        [SerializeField] private Image correctAnswerFeedback;
        [SerializeField] private Image wrongAnswerFeedback;
        [SerializeField] private Button categoryButton;
        [SerializeField] private Image focusImage;
        [SerializeField] private Image additionalFocusImage;

        public void Init(Action<CategoryView> onSelected, string category)
        {
            categoryButton.OnClickAsObservable().Subscribe(_ => onSelected(this));
            categoryText.text = category;
            userResponseText.text = "";
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
            Reset();
        }

        public string GetCurrentText()
        {
            return userResponseText.text;
        }

        public void SetResponse(string response)
        {
            userResponseText.text = response;
        }

        public void SetCorrectFeedback()
        {
            userResponseText.color = Color.black;
            correctAnswerFeedback.gameObject.SetActive(true);
        }

        public void SetIncorrectFeedback()
        {
            userResponseText.color = Color.red;
            wrongAnswerFeedback.gameObject.SetActive(true);
        }

        private void Reset()
        {
            userResponseText.color = Color.black;
            correctAnswerFeedback.gameObject.SetActive(false);
            wrongAnswerFeedback.gameObject.SetActive(false);
        }

        public void ShowSelectionFeedback()
        {
            focusImage.gameObject.SetActive(true);
            additionalFocusImage.gameObject.SetActive(true);
        }

        public void HideSelectionFeedback()
        {
            focusImage.gameObject.SetActive(false);
            additionalFocusImage.gameObject.SetActive(false);
        }
    }
}