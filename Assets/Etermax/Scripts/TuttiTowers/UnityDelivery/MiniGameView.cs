﻿using System;
using Etermax.Scripts.ProteoTemplate.UnityDelivery.Views;
using Etermax.Scripts.TuttiTowers.Presentation;
using Etermax.Scripts.TuttiTowers.Presentation.Views;
using Etermax.Scripts.TuttiTowers.Services;
using Etermax.Scripts.Utils;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Etermax.Scripts.TuttiTowers.UnityDelivery
{
    public class MiniGameView : MonoBehaviour, IMiniGameView
    {
        [SerializeField] private TextMeshProUGUI _characterText;
        [SerializeField] private CategoryView[] _categories;
        [SerializeField] private TimerView _timerView;
        [SerializeField] private AcceptPopupView _timeOutPopup;
        [SerializeField] private KeyboardView keyboardView;
        [SerializeField] private Button stopButton;
        [SerializeField] private Image timerProgressBar;
        [SerializeField] private TextMeshProUGUI timerText;

        private MiniGamePresenter _presenter;
        private IDisposable _keyboardUpdateDisposable;
        private IDisposable _keyboardDisposable;
        private const bool CORRECT_ANSWER = true;

        public void Init(IMiniGameValidatorService service, Action<int> onEnded)
        {
            _presenter = new MiniGamePresenter(this, service, onEnded);
            _timeOutPopup.Hide();
            foreach (var category in _categories)
                category.Hide();

            stopButton.OnClickAsObservable().Subscribe(_ => OnStopButtonPressed());
            
            keyboardView.Init(OnKeyboardSubmit);
            keyboardView.DisableSubmitButtonAutoToggle();
            keyboardView.ToggleInputContainer(false);
           
        }

        public void StartMiniGame(MiniGameData data)
        {
            if (_keyboardDisposable != null) _keyboardDisposable.Dispose();
            _timeOutPopup.Hide();
            _presenter.Init(data);
            _keyboardDisposable = keyboardView.GetCurrentInputObservable().Do(OnKeyboardInputUpdate).Subscribe();
        }

        public void ShowCharacter(string character)
        {
            _characterText.text = character;
        }

        public void ShowAllCategories(string[] categories)
        {
            for (int i = 0; i < categories.Length; i++)
            {
                _categories[i].Show();
                _categories[i].HideSelectionFeedback();
                _categories[i].Init(OnCategoryPressed, categories[i]);
            }
        }

        public void ShowUserResponseOnCategory(int categoryIndex, string response)
        {
            _categories[categoryIndex].SetResponse(response);
        }
        public void ShowTimerEndFeedback(int correctAnswersCount)
        {
            _timeOutPopup.Init(_presenter.OnMiniGameEnded, "Go Back", "Time Ended", "You now have " + correctAnswersCount + " rolls");
            _timeOutPopup.Show();
        }

        public void ShowAnswersFeedback(bool[] answers)
        {
            for (int i = 0; i < answers.Length; i++)
            {
                if (answers[i] == CORRECT_ANSWER)
                {
                    _categories[i].SetCorrectFeedback();
                }
                else
                {
                    _categories[i].SetIncorrectFeedback();
                }
            }
        }

        public void ShowSelectionFeedbackOn(int categoryIndex)
        {
            var categoryView = _categories[categoryIndex];
            var currentText = categoryView.GetCurrentText();
            categoryView.ShowSelectionFeedback();
            keyboardView.Show(25, "");
            keyboardView.OnSetDefaultText(currentText);
        }

        private void OnKeyboardInputUpdate(string s)
        {
            _presenter.OnUpdateText(s);
        }

        public void HideSelectionFeedbackOn(int categoryIndex)
        {
            _categories[categoryIndex].HideSelectionFeedback();
        }

        public void UpdateTimer(long timerProgress, int dataMaxTime)
        {
            timerText.text = (dataMaxTime - timerProgress).ToString();
            timerProgressBar.fillAmount = 1 - timerProgress / (float) dataMaxTime;
        }

        private void OnCategoryPressed(CategoryView category)
        {
            _presenter.OnCategorySelected(_categories.IndexOf(c => c.Equals(category)));
        }

        private void OnKeyboardSubmit(string keyboardInput)
        {
            _presenter.OnSubmit(keyboardInput);
        }

        private void OnStopButtonPressed()
        {
            _timerView.Stop();
            _presenter.OnFinishTimer();
        }
    }
}
