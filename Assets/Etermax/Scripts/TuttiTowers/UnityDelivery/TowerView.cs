using Etermax.Scripts.TuttiTowers.Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Etermax.Scripts.TuttiTowers.UnityDelivery
{
    public class TowerView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private Image flagImage;
        [SerializeField] private RectTransform highlight;
        [SerializeField] private Animator lightAnimation;
        private static readonly int On = Animator.StringToHash("ON");

        public void SetScore(int score)
        {
            scoreText.text = score.ToString();
        }

        public void SetColor(Color color)
        {
            flagImage.color = color;
        }

        public void ShowHighlight()
        {
            lightAnimation.gameObject.SetActive(true);
            lightAnimation.SetTrigger(On);
            highlight.gameObject.SetActive(true);
        }

        public void HideHighlight()
        {
            lightAnimation.gameObject.SetActive(false);
            highlight.gameObject.SetActive(false);
        }
    }
}