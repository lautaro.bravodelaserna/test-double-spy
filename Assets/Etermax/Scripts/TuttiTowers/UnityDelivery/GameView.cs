using System;
using System.Collections.Generic;
using System.Linq;
using Etermax.Scripts.ProteoTemplate.UnityDelivery.Views;
using Etermax.Scripts.ProteoTemplate.UnityDelivery.Views.GameplayHeader;
using Etermax.Scripts.TuttiLadders.Services;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Presentation;
using Etermax.Scripts.TuttiTowers.Presentation.Views;
using Etermax.Scripts.Utils;
using Etermax.Utilities;
using Etermax.Utilities.Rx;
using Package.Scripts;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Etermax.Scripts.TuttiTowers.UnityDelivery
{
    public class GameView : MonoBehaviour, IGameView
    {
        [SerializeField] private TileView[] tiles;
        [SerializeField] private TowerView[] towers;
        [SerializeField] private TowerTileView towerTilePrefab;
        [SerializeField] private TextMeshProUGUI diceRollsAvailableText;
        [SerializeField] private RectTransform rollContainer;
        [SerializeField] private Button rollButton;
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform player;
        [SerializeField] private RectTransform opponent;
        [SerializeField] private RectTransform temporalPlayerParent;
        [SerializeField] private Color defaultFlagColor;
        [SerializeField] private Color playerColor;
        [SerializeField] private Color opponentColor;
        [SerializeField] private GameplayHeaderView header;
        [SerializeField] private AcceptPopupView winPopup;

        private GamePresenter _presenter;
        private List<TowerTileView> _towerPlatformInstances;

        public void Init(IDiceService diceService, Action onPlayStarted, Action<int> onFightStarted, Action<TurnEndData> onTurnEnded)
        {
            _towerPlatformInstances = new List<TowerTileView>();

            _presenter = new GamePresenter(this, diceService, onPlayStarted, onFightStarted, onTurnEnded);

            rollButton.OnClickAsObservable().Subscribe(_ => _presenter.RollButtonPressed());
            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(KeyCode.Alpha1))
                .Do(_ => _presenter.IncreaseRollCount())
                .Subscribe();
            startButton.OnClickAsObservable().Subscribe(_ => _presenter.OnStartButtonPressed());
            
            winPopup.Init(() => { SoundManager.PlaySound(SoundType.Win, Random.Range(1, 1.5f)); }, "Festejar", "Felicidades", "Sos el ganador");
        }

        public void StartGame(GameData gameData)
        {
            header.Setup(gameData.scoreToWin, false);

            foreach (var towerPlatformInstance in _towerPlatformInstances)
            {
                Destroy(towerPlatformInstance.gameObject);
            }
            _towerPlatformInstances.Clear();
            
            gameData.boardSize = tiles.Length;
            _presenter.StartGame(gameData);
        }

        public void OnPlayEnded(int rollCount)
        {
            _presenter.OnPlayEnded(rollCount);
        }

        public void OnFightEnded(int score)
        {
            _presenter.OnFightEnded(score);
        }

        public void ShowBoard()
        {
            /*HideBoard();
            
            var orderedTiles = tiles.OrderBy(t => t.GetIndex()).ToArray();
            
            for (var i = 0; i < orderedTiles.Length; i++)
            {
                var tile = orderedTiles[i];
                Observable.Timer(TimeSpan.FromMilliseconds(1000 + i * 75)).Subscribe(_ => tile.AnimateSpawn());
            }

            var shuffledTowers = towers.Shuffle().ToArray();
            for (var i = 0; i < shuffledTowers.Length; i++)
            {
                var tower = shuffledTowers[i];
                tower.gameObject.SetActive(false);
                Observable.Timer(TimeSpan.FromMilliseconds(1750 + i * 300))
                    .Subscribe(_ => tower.gameObject.SetActive(true));
            }*/
        }

        public void ShowCategoryCellsAt(int indexPosition, CategoryCell categoryCell)
        {
            var tile = GetTileAt(indexPosition);
            var towerInstance = Instantiate(towerTilePrefab, tile.transform);
            tile.AddTowerPlatform(towerInstance.transform);
            towerInstance.transform.localPosition = Vector3.zero;
            _towerPlatformInstances.Add(towerInstance);
        }

        public void UpdatePlayerAvailableDiceRolls(int moves)
        {
            diceRollsAvailableText.text = moves.ToString();
        }

        public void ShowRollButton()
        {
            rollContainer.gameObject.SetActive(true);
        }

        public void HideRollButton()
        {
            rollContainer.gameObject.SetActive(false);
        }

        public void SetPlayerPosition(int indexPosition)
        {
            var targetTile = GetTileAt(indexPosition);
            targetTile.AddChildToPlatform(player);
            player.transform.localPosition = Vector3.zero;
        }

        public void SetOpponentPosition(int indexPosition)
        {
            var targetTile = GetTileAt(indexPosition);
            targetTile.AddChildToPlatform(opponent);
            opponent.transform.localPosition = Vector3.zero;
        }

        public IObservable<float> MovePlayerTowards(int fromIndex, int toIndex)
        {    
            var fromTile = GetTileAt(fromIndex);
            var toTile = GetTileAt(toIndex);

            return Observable.Return(0f)
                .Do(_ => player.transform.SetParent(temporalPlayerParent.transform))
                .Concat(
                    RxTween.Make(0, 1, 0.5f, Easings.QuintOut)
                        .Do(v => MovePlayerTowardsPosition(fromTile, toTile, v))
                        .DoOnCompleted(() => player.transform.SetParent(toTile.transform)));
                
        }

        public void ShowTowerHighlight(int towerIndex)
        {
            towers[towerIndex].ShowHighlight();
        }

        public void HideTowerHighlight(int towerIndex)
        {
            towers[towerIndex].HideHighlight();
        }

        private Vector3 MovePlayerTowardsPosition(TileView fromTile, TileView toTile, float lerpValue)
        {
            return player.transform.position = Vector3.Lerp(fromTile.transform.position, toTile.transform.position, lerpValue);
        }

        public void ShowWinPopup()
        {
            winPopup.Show();
        }

        public void ShowStartButton()
        {
            startButton.gameObject.SetActive(true);
        }

        public void HideStartButton()
        {
            startButton.gameObject.SetActive(false);
        }

        private void HideBoard()
        {
            foreach (var tile in tiles)
            {
                tile.Hide();
            }
        }

        private TileView GetTileAt(int indexPosition)
        {
            return tiles.First(t => t.GetIndex() == indexPosition);
        }

        public void SetCellOwnerAsPlayer(int towerIndex)
        {
            towers[towerIndex].SetColor(playerColor);
        }

        public void SetCellOwnerAsOpponent(int towerIndex)
        {
            towers[towerIndex].SetColor(opponentColor);
        }

        public void SetFlagAsDefault(int towerIndex)
        {
            towers[towerIndex].SetColor(defaultFlagColor);
        }

        public void SetCellScore(int towerIndex, int score)
        {
            towers[towerIndex].SetScore(score);
        }
        public void UpdatePlayerScore(int playerScore)
        {
            header.UpdateScore(playerScore, 1);
        }

        public void InitializeScores(int playerScore, int opponentScore)
        {
            header.InitializeScores(playerScore, opponentScore);
        }
    }
}