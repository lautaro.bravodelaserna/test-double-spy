using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Etermax.Scripts.TuttiTowers.UnityDelivery
{
    public class TileView : MonoBehaviour
    {
        [SerializeField] private RectTransform tileViewPrefab;
        [SerializeField] private RectTransform imageTransform;
        [SerializeField] private RectTransform pivot;
        [SerializeField] private Image imageComponent;
        [SerializeField] private int tileIndex = 0;
        [SerializeField] private Animator anim;
        [SerializeField] private Sprite[] imageVariants;
        [SerializeField] private RectTransform towerPlatformContainer;
        
        private static readonly int Spawn = Animator.StringToHash("Spawn");
        private static readonly int Hidden = Animator.StringToHash("Hidden");

        public void Awake()
        {
            imageComponent.sprite = imageVariants[Random.Range(0, imageVariants.Length)];
            var scale = Vector3.one;
            scale.x = Random.Range(0, 2) > 0 ? 1 : -1;
            imageTransform.localScale = scale;
        }

        public int GetIndex()
        {
            return tileIndex;
        }

        public void SetIndex(int index)
        {
            tileIndex = index;
        }

        public void Hide()
        {
            anim.SetTrigger(Hidden);
        }

        public void AnimateSpawn()
        {
            anim.SetTrigger(Spawn);
        }

        public void AddChildToPlatform(Transform child)
        {
            child.SetParent(pivot);
        }

        public void AddTowerPlatform(Transform platform)
        {
            platform.SetParent(towerPlatformContainer);
        }
    }
    
    #if UNITY_EDITOR

    [CustomEditor(typeof(TileView))]
    public class TileViewInspector : Editor
    {
        private static bool _duplicateOnMove = false;
        private static Vector2 _customOffset;
        public override void OnInspectorGUI()
        {
            TileView tileView = (TileView) target;

            _customOffset = EditorGUILayout.Vector2Field("Offset: ", _customOffset);
            
            DrawChildUp(tileView);
            DrawChildDown(tileView);
            DrawRepositionButtons(tileView);

            _duplicateOnMove = GUILayout.Toggle(_duplicateOnMove, "Duplicate on move");
            
            base.OnInspectorGUI();
        }

        private void DrawChildUp(TileView tileView)
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Child ^", GUILayout.MaxWidth(50)))
            {
                Undo.RegisterFullObjectHierarchyUndo(tileView.transform, "Child-");
                tileView.transform.SetSiblingIndex(tileView.transform.GetSiblingIndex() - 1);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private void DrawChildDown(TileView tileView)
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Child v", GUILayout.MaxWidth(50)))
            {
                Undo.RegisterFullObjectHierarchyUndo(tileView.transform, "Child+");
                tileView.transform.SetSiblingIndex(tileView.transform.GetSiblingIndex() + 1);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private void DrawRepositionButtons(TileView tileView)
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("↖", GUILayout.MaxWidth(25)))
            {
                var direction = _customOffset.x * Vector3.left + _customOffset.y * Vector3.up;
                MoveTileTowards(tileView, direction, true);
            }

            if (GUILayout.Button("↗", GUILayout.MaxWidth(25)))
            {
                var direction = _customOffset.x * Vector3.right + _customOffset.y * Vector3.up;
                MoveTileTowards(tileView, direction, true);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("↙", GUILayout.MaxWidth(25)))
            {
                var direction = _customOffset.x* Vector3.left + _customOffset.y * Vector3.down;
                MoveTileTowards(tileView, direction, false);
            }

            if (GUILayout.Button("↘", GUILayout.MaxWidth(25)))
            {
                var direction = _customOffset.x * Vector3.right +_customOffset.y * Vector3.down;
                MoveTileTowards(tileView, direction, false);

            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private void MoveTileTowards(TileView tileView, Vector3 direction, bool moveChildUp)
        {
            var childDirection = moveChildUp ? -1 : 1;
            var transformToMove = tileView.transform;

            if (_duplicateOnMove)
            {
                var boardTile = Resources.Load<RectTransform>("BoardTile");
                transformToMove = (RectTransform)PrefabUtility.InstantiatePrefab(boardTile, tileView.transform.parent);
                Undo.RegisterCreatedObjectUndo(transformToMove.gameObject, "DuplicatedTile");

                transformToMove.SetSiblingIndex(tileView.transform.GetSiblingIndex() + childDirection);
                transformToMove.transform.position = tileView.transform.position;
                
                transformToMove.GetComponent<TileView>().SetIndex(tileView.GetIndex() + 1);
            }
            else
            {
                Undo.RecordObject(tileView.transform, "MoveWithNoDuplication");
                transformToMove.SetSiblingIndex(transformToMove.GetSiblingIndex() + childDirection);
            }
            
            transformToMove.localPosition += direction;

            if (_duplicateOnMove)
            {
                Selection.activeObject = transformToMove;
                Repaint();
            }
        }
    }
    
    
    #endif
}