using System.Collections.Generic;

namespace Etermax.Scripts.TuttiTowers.Services
{
    public interface ITowerMiniGameValidatorService
    {
        bool Validate(string category, string word);
        List<string> GetWords(string category);
    }
}