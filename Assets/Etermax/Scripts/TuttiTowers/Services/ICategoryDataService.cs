using System.Collections.Generic;

namespace Etermax.Scripts.TuttiTowers.Services
{
    public interface ICategoryDataService
    {
        Dictionary<string, List<string>> GetWordsByCategory();
        char GetRandomValidChar();
        string[] GetRandomCategories(char startingChar);
        string GetRandomCategory();
    }
}