using System.Collections.Generic;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.Utils;
using UnityEngine;

namespace Etermax.Scripts.TuttiTowers.Services
{
    public class MiniGameValidatorService : IMiniGameValidatorService
    {
        private readonly ICategoryDataService _categoryDataService;

        public MiniGameValidatorService(ICategoryDataService categoryDataService)
        {
            _categoryDataService = categoryDataService;
        }

        public bool[] Validate(string startingLetter, string[] userInputs, string[] categories)
        {
            List<bool> validations = new List<bool>();

            for (var i = 0; i < userInputs.Length; i++)
            {
                if (string.IsNullOrEmpty(userInputs[i]))
                {
                    validations.Add(false);
                    continue;
                }
                
                var userInput = userInputs[i];
                var category = categories[i];
                var trimmedWord = userInput.Trim();
                var isValid = IsWordStartingWith(trimmedWord, startingLetter) && WordExistsOnCategory(userInput, category);

                validations.Add(isValid);
            }

            return validations.ToArray();
        }

        private bool WordExistsOnCategory(string word, string category)
        {
            return _categoryDataService.GetWordsByCategory()[category].Contains(word);
        }

        private static bool IsWordStartingWith(string trimmedWord, string startingLetter)
        {
            return trimmedWord[0] == startingLetter[0];
        }


    }
}