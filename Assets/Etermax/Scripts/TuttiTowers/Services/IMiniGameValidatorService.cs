using System.Collections.Generic;

namespace Etermax.Scripts.TuttiTowers.Services
{
    public interface IMiniGameValidatorService
    {
        bool[] Validate(string startingLetter, string[] userInputs, string[] categories);
    }
}