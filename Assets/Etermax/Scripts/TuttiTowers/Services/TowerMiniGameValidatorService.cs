using System.Collections.Generic;

namespace Etermax.Scripts.TuttiTowers.Services
{
    public class TowerMiniGameValidatorService : ITowerMiniGameValidatorService
    {
        private readonly CategoryDataService _categoryDataService;

        public TowerMiniGameValidatorService(CategoryDataService categoryDataService)
        {
            _categoryDataService = categoryDataService;
        }

        public bool Validate(string category, string word)
        {
            var wordsByCategory = _categoryDataService.GetWordsByCategory();
            return wordsByCategory[category].Contains(word.Trim());
        }

        public List<string> GetWords(string category)
        {
            return _categoryDataService.GetWordsByCategory()[category];
        }
    }
}