using Etermax.Scripts.Services.Random;
using Etermax.Scripts.TuttiLadders.Services;

namespace Etermax.Scripts.TuttiTowers.Services
{
    public class DiceService : IDiceService
    {
        private readonly IRandomService _randomService;

        public DiceService(IRandomService randomService)
        {
            _randomService = randomService;
        }

        public int Roll()
        {
            return _randomService.GetRandomNumber(1, 7);
        }
    }
}