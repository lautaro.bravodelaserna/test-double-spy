namespace Etermax.Scripts.TuttiLadders.Services
{
    public interface IDiceService
    {
        int Roll();
    }
}