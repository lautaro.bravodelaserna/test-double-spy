using System.Linq;
using Etermax.Scripts.Services.Random;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.Utils;

namespace Etermax.Scripts.TuttiTowers.Services
{
    public struct PlayerData
    {
        public int id;
        public int position;
    }
    public class Server
    {
        private readonly IRandomService _randomService;
        private readonly CategoryDataService _categoryDataService;
        private GameData _gameData;
        private bool _isGameStarted;
        private PlayerData[] _players;
        private int _currentPlayerIndex;

        public Server(IRandomService randomService, CategoryDataService categoryDataService)
        {
            _randomService = randomService;
            _categoryDataService = categoryDataService;
            _isGameStarted = false;
            
        }

        public GameData GetGameData()
        {
            if (!_isGameStarted)
            {
                CreateGameData();
                _isGameStarted = true;
            }

            _gameData.playerPosition = _players[_currentPlayerIndex].position;
            _gameData.opponentPosition = _players[GetNextPlayerIndex()].position;
            _gameData.playerId = _players[_currentPlayerIndex].id;
            
            return _gameData;
        }

        public MiniGameData GetNewMiniGameData()
        {
            var randomCharacter = _categoryDataService.GetRandomValidChar();
            var miniGameData = new MiniGameData
            {
                categories = _categoryDataService.GetRandomCategories(randomCharacter),
                maxTime = 99,
                character = randomCharacter.ToString().ToUpper()
            };

            return miniGameData;
        }

        public void OnTurnEnded(TurnEndData data)
        {
            _players[_currentPlayerIndex].position = data.currentPlayerPosition;
            _gameData.cells = data.towers.ToArray();

            _currentPlayerIndex = GetNextPlayerIndex();
        }

        private int GetNextPlayerIndex()
        {
            return (_currentPlayerIndex + 1) % _players.Length;
        }

        private void CreateGameData()
        {
            _currentPlayerIndex = 0;
            _players = new[]
            {
                new PlayerData {id = 1, position = 0},
                new PlayerData {id = 2, position = 8}
            };
            
            _gameData = new GameData
            {
                cells = new[]
                {
                    new CellData { id = 0, positions = new []{0}, score = 5},
                    new CellData { id = 0, positions = new []{1}, score = 5},
                    new CellData { id = 0, positions = new []{2}, score = 5},
                    new CellData { id = 0, positions = new []{3}, score = 5},
                    new CellData { id = 0, positions = new []{4}, score = 5},
                    new CellData { id = 0, positions = new []{5}, score = 5},
                    new CellData { id = 0, positions = new []{6}, score = 5},
                    new CellData { id = 0, positions = new []{7}, score = 5},
                    new CellData { id = 0, positions = new []{8}, score = 5},
                    new CellData { id = 0, positions = new []{9}, score = 5},
                    new CellData { id = 0, positions = new []{10}, score = 5},
                    new CellData { id = 0, positions = new []{11}, score = 5},
                    new CellData { id = 0, positions = new []{12}, score = 5},
                    new CellData { id = 0, positions = new []{13}, score = 5},
                    new CellData { id = 0, positions = new []{14}, score = 5},
                    new CellData { id = 0, positions = new []{15}, score = 5},
                },
                playerPosition = 0,
                opponentPosition = 0,
                playerId = 1,
                scoreToWin = 8
            };
        }

        public TowerMiniGameData GetNewTowerMiniGameData()
        {
            return new TowerMiniGameData()
            {
                Category = _categoryDataService.GetRandomCategory(),
                MaxTime = 60
            };
        }
    }
}