using System.Collections.Generic;
using System.Linq;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.Utils;

using Letter = System.Char;
using Category = System.String;
using Word = System.String;

using UnityEngine;

namespace Etermax.Scripts.TuttiTowers.Services
{
    public class CategoryDataService : ICategoryDataService
    {
        private Dictionary<Category, List<Word>> _wordsByCategoryFiltered;
        private Dictionary<Letter, List<Category>> _filteredCategoriesPerLetter;

        public CategoryDataService(WordItemData[] gameData)
        {
            ParseDataIntoTable(gameData);
        }

        public Dictionary<string, List<string>> GetWordsByCategory()
        {
            return _wordsByCategoryFiltered;
        }

        public char GetRandomValidChar()
        {
            return _filteredCategoriesPerLetter.Select(kvp => kvp.Key).Shuffle().First();
        }
        public string[] GetRandomCategories(char startingChar)
        {
            return _filteredCategoriesPerLetter[startingChar].ToArray().Shuffle().Take(6).ToArray();
        }

        public string GetRandomCategory()
        {
            return _wordsByCategoryFiltered.Select(kvp => kvp.Key).Shuffle().First();
        }
        
        private void ParseDataIntoTable(WordItemData[] gameData)
        {
            var filteredWordsByStartingLetter = GetUsableWords(gameData, 20);

            var wordsByCategory = GetWordsByCategory(filteredWordsByStartingLetter);

            _wordsByCategoryFiltered = wordsByCategory
                .Where( wbc => wbc.Value.Count > 20)
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            _filteredCategoriesPerLetter = ArrangeCategoriesPerStartingLetter();
            
        }

        private Dictionary<char, List<string>> ArrangeCategoriesPerStartingLetter()
        {
            var categoriesPerLetter = new Dictionary<Letter, List<Category>>();
            foreach (var wordByCategory in _wordsByCategoryFiltered)
            {
                foreach (Word word in wordByCategory.Value)
                {
                    var startingLetter = word.GetClean().ToUpper()[0];
                    if (!categoriesPerLetter.ContainsKey(startingLetter))
                    {
                        categoriesPerLetter.Add(startingLetter, new List<Category>());
                    }

                    if (!categoriesPerLetter[startingLetter].Contains(wordByCategory.Key))
                        categoriesPerLetter[startingLetter].Add(wordByCategory.Key);
                }
            }

            var filteredCategoriesPerLetter = categoriesPerLetter.Where(cpl => cpl.Value.Count >= 6)
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            return filteredCategoriesPerLetter;
        }

        private static Dictionary<string, List<string>> GetWordsByCategory(Dictionary<char, List<WordItemData>> filteredWordsByStartingLetter)
        {
            var wordsByCategory = new Dictionary<Category, List<Word>>();
            var flattenedWordList = new List<WordItemData>();
            foreach (var kvp in filteredWordsByStartingLetter)
            {
                flattenedWordList.AddRange(kvp.Value);
            }

            foreach (var wordItemData in flattenedWordList)
            {
                foreach (string tag in wordItemData.Tags)
                {
                    var upperTag = tag.ToUpper();
                    if (!wordsByCategory.ContainsKey(upperTag))
                    {
                        wordsByCategory.Add(upperTag, new List<string>());
                    }

                    wordsByCategory[upperTag].Add(wordItemData.Word.ToUpper().Trim());
                }
            }

            return wordsByCategory;
        }

        private static Dictionary<char, List<WordItemData>> GetUsableWords(WordItemData[] gameData, int minAmmountOfWordsWithSameStartingLetter)
        {
            var wordsByStartingLetter = new Dictionary<Letter, List<WordItemData>>();
            foreach (var wordItemData in gameData)
            {
                var firstChar = wordItemData.Word.ToUpper()[0];
                if (!wordsByStartingLetter.ContainsKey(firstChar))
                {
                    wordsByStartingLetter.Add(firstChar, new List<WordItemData>());
                }

                wordsByStartingLetter[firstChar].Add(wordItemData);
            }

            var filteredWordsByStartingLetter = wordsByStartingLetter.Where(w => w.Value.Count >= minAmmountOfWordsWithSameStartingLetter)
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            return filteredWordsByStartingLetter;
        }
    }
}