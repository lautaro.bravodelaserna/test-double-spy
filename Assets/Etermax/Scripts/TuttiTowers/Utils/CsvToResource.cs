
using System.IO;
using System.Linq;
using Etermax.Scripts.TuttiTowers.Model;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
using System.Collections.Generic;

using Word = System.String;
using Category = System.String;

namespace Etermax.Scripts.TuttiTowers.Utils
{
    public static class CsvToResource
    {
        [MenuItem("Tools/ProcessCSV")]
        public static void GenerateDataFromCrossFiles()
        {
            string dataPath = "Assets/Etermax/Resources/";

            List<string> wordFilesList = GetFileList();

            Dictionary<Word, List<Category>> outputData = new Dictionary<Word, List<Category>>();

            List<WordItemData> bakedData = new List<WordItemData>();

            wordFilesList.ForEach(file => { GetDataFromFile(dataPath, file, outputData, bakedData); });

            BakeDataFromOutput(bakedData, dataPath);
        }

        private static void GetDataFromFile(string dataPath, string file, Dictionary<string, List<string>> outputData, List<WordItemData> bakedData)
        {
            string path = dataPath + file + ".csv";
            string category = file;

            StreamReader reader = new StreamReader(path);

            ExtractWordsFromFileAndCreateOutputData(reader, outputData, category, bakedData);

            reader.Close();
        }

        private static void BakeDataFromOutput(List<WordItemData> bakedData, string dataPath)
        {
            ParsedData asset = ScriptableObject.CreateInstance<ParsedData>();
            asset.WordItems = bakedData.ToArray();
            AssetDatabase.CreateAsset(asset, dataPath + "GameData.asset");
            AssetDatabase.SaveAssets();
        }

        private static void ExtractWordsFromFileAndCreateOutputData(StreamReader reader, Dictionary<string, List<string>> outputData, string category, List<WordItemData> bakedData)
        {
            while (!reader.EndOfStream)
            {
                var words = ExtractWordsFromLine(reader);

                FillOutputDataWithWordsFromLine(words, outputData, category);

                BakeDataFromFileOutput(outputData, bakedData);
            }
        }

        private static void BakeDataFromFileOutput(Dictionary<string, List<string>> outputData, List<WordItemData> bakedData)
        {
            outputData.ToList().ForEach(data => { bakedData.Add(new WordItemData(data.Key, data.Value.ToArray())); });
        }

        private static void FillOutputDataWithWordsFromLine(string[] words, Dictionary<string, List<string>> outputData, string category)
        {
            words.ToList().ForEach(word =>
            {
                if (string.IsNullOrEmpty(word)) return;

                word = RemoveInitialLetterWithColon(word);

                if (!outputData.ContainsKey(word))
                {
                    outputData[word] = new List<string>();
                }

                outputData[word].Add(category);
            });
        }

        private static string RemoveInitialLetterWithColon(string word)
        {
            string[] wordWithInitialLetter = word.Split(':');
            if (wordWithInitialLetter.Length > 1)
            {
                word = wordWithInitialLetter[1];
            }

            return word.ToUpper();
        }

        private static string[] ExtractWordsFromLine(StreamReader reader)
        {
            return reader.ReadLine().Split(',');
        }

        private static List<string> GetFileList()
        {
            return new List<string>()
            {
                "animales",
                "bandas",
                "libros",
                "marcas",
                "capitales",
                "autos",
                "celebridades",
                "ropa",
                "colores",
                "paises",
                "disney",
                "perros",
                "elementos",
                "personajes",
                "trabajos",
                "idiomas",
                "peliculas",
                "objetos",
                "tv",
                "deportes",
                "superheroes",
                "superpoderes",
                "pokemon",
                "veggie",
                "videojuegos",
                "youtuber"
            };
        }
    }
}
#endif