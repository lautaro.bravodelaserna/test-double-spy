using Etermax.Scripts.TuttiTowers.Model;
using UniRx;

namespace Etermax.Scripts.TuttiTowers.Presentation.Views
{
    public interface IGameView
    {
        void ShowBoard();
        void ShowCategoryCellsAt(int indexPosition, CategoryCell categoryCell);
        void UpdatePlayerAvailableDiceRolls(int moves);
        void ShowRollButton();
        void HideRollButton();
        void SetPlayerPosition(int indexPosition);
        void SetOpponentPosition(int indexPosition);

        void ShowWinPopup();
        void ShowStartButton();
        void HideStartButton();
        IObservable<float> MovePlayerTowards(int fromIndex, int toIndex);
        void ShowTowerHighlight(int towerIndex);
        void HideTowerHighlight(int towerIndex);
        void SetCellOwnerAsPlayer(int towerIndex);
        void SetCellOwnerAsOpponent(int towerIndex);
        void SetFlagAsDefault(int towerIndex);
        void SetCellScore(int towerIndex, int score);
        void UpdatePlayerScore(int playerScore);
        void InitializeScores(int playerScore, int opponentScore);
    }
}