﻿using System;

namespace Etermax.Scripts.TuttiTowers.Presentation.Views
{
    public interface IMiniGameView
    {
        void ShowCharacter(string character);
        void ShowAllCategories(string[] categories);
        void ShowUserResponseOnCategory(int categoryIndex, string response);
        void ShowTimerEndFeedback(int correctAnswersCount);
        void ShowAnswersFeedback(bool[] answers);
        void ShowSelectionFeedbackOn(int categoryIndex);
        void HideSelectionFeedbackOn(int categoryIndex);
        void UpdateTimer(long timerProgress, int dataMaxTime);
    }
}
