namespace Etermax.Scripts.TuttiTowers.Presentation.Views
{
    public interface ITowerMiniGameView
    {
        void ShowCategory(string categoryName);
        void ShowTimer(float maxTime);
        void ShowWordInput(string wordInput);
        void ShowValidFeedback();
        void ShowInvalidFeedback();
        void ShowResultScreen(int score);
        void SetHealth(int healthPoints);
    }
}