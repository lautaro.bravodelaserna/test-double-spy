﻿using System;
using System.Linq;
using System.Collections.Generic;
using Etermax.Scripts.TuttiLadders.Services;
using Etermax.Scripts.TuttiTowers.Presentation.Views;
using Etermax.Scripts.TuttiTowers.Services;
using UniRx;

namespace Etermax.Scripts.TuttiTowers.Presentation
{
    public class MiniGamePresenter
    {
        private MiniGameData _data;
        private IMiniGameView _view;
        private readonly IMiniGameValidatorService _validatorService;

        private string[] _userInput;
        private Action<int> _onMiniGameEnded;
        private int _correctAnswersCount;
        private int _lastSelectedCategory;
        private IDisposable _currentTimer;

        public MiniGamePresenter(IMiniGameView view, IMiniGameValidatorService validatorService, Action<int> onMiniGameEnded)
        {
            _view = view;
            _validatorService = validatorService;
            _onMiniGameEnded = onMiniGameEnded;
        }

        public void Init(MiniGameData data)
        {
            _lastSelectedCategory = 0;
            _correctAnswersCount = 0;
            _data = data;
            _userInput = new string[data.categories.Length];
            _view.ShowCharacter(data.character);
            _view.ShowAllCategories(data.categories);
            StartTimer(data.maxTime);
            OnCategorySelected(0);
        }

        private void StartTimer(int maxTime)
        {
            //_view.StartTimer(maxTime);
            _currentTimer = Observable.Timer(TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(1))
                .TakeWhile(t => t <= maxTime)
                .Subscribe(OnTimerTick, OnFinishTimer);
        }

        private void OnTimerTick(long timerProgress)
        {
            _view.UpdateTimer(timerProgress, _data.maxTime);
        }

        public void OnSubmit(string response)
        {
            OnUpdateText(response);
            SelectNextEmptyCategory();
        }

        public void OnUpdateText(string response)
        {
            _userInput[_lastSelectedCategory] = response.Trim().Replace("_", " ");
            _view.ShowUserResponseOnCategory(_lastSelectedCategory, response.Trim());
        }

        public void OnFinishTimer()
        {
            _currentTimer.Dispose();
            var answers = _validatorService.Validate(_data.character, _userInput, _data.categories);
            _correctAnswersCount = answers.Count(a => a)  + 1;

            _view.ShowAnswersFeedback(answers);

            ExecuteTimer(3f, () =>
            {
                _view.ShowTimerEndFeedback(_correctAnswersCount);
            } );
        }

        protected virtual void ExecuteTimer(float seconds, Action action)
        {
            Observable.Timer(TimeSpan.FromSeconds(seconds)).Subscribe(_ => { action(); });
        }

        public void OnMiniGameEnded()
        {
            _onMiniGameEnded(_correctAnswersCount);
        }

        public void OnCategorySelected(int categoryIndex)
        {
            _view.HideSelectionFeedbackOn(_lastSelectedCategory);
            _lastSelectedCategory = categoryIndex;
            _view.ShowSelectionFeedbackOn(_lastSelectedCategory);
        }

        private void SelectNextEmptyCategory()
        {
            for (int i = 1; i < _userInput.Length; i++)
            {
                var currentPointer = (_lastSelectedCategory + i) % _userInput.Length;
                if (IsInputSkippable(currentPointer)) continue;
                    
                OnCategorySelected(currentPointer);
                return;
            }
            
            _view.HideSelectionFeedbackOn(_lastSelectedCategory);
            OnFinishTimer();
        }

        private bool IsInputSkippable(int currentPointer)
        {
            if (string.IsNullOrEmpty(_userInput[currentPointer])) return false;
            return !string.IsNullOrEmpty(_userInput[currentPointer].Trim());
        }
    }
}
