using System;
using System.Collections.Generic;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Presentation.Views;
using Etermax.Scripts.TuttiTowers.Services;

namespace Etermax.Scripts.TuttiTowers.Presentation
{
    public class TowerMiniGamePresenter
    {
        private readonly ITowerMiniGameView _view;
        private readonly ITowerMiniGameValidatorService _service;
        private readonly Action<int> _onMiniGameEnded;
        private readonly List<string> _inputs;
        private string _categoryName;
        private int _score;
        private int _scoreToBeat;

        public TowerMiniGamePresenter(ITowerMiniGameView view, ITowerMiniGameValidatorService service, Action<int> onMiniGameEnded)
        {
            _view = view;
            _service = service;
            _onMiniGameEnded = onMiniGameEnded;
            _inputs = new List<string>();
        }

        public void Init(TowerMiniGameData gameData, int scoreToBeat)
        {
            _scoreToBeat = scoreToBeat;
            _categoryName = gameData.Category;
            _view.ShowCategory(_categoryName);
            _view.ShowTimer(gameData.MaxTime);
            _view.SetHealth(scoreToBeat);
            ResetData();
        }

        public void OnWordInput(string wordInput)
        {
            wordInput = wordInput.Trim().Replace("_", " ");

            if (!_inputs.Contains(wordInput))
            {
                _view.ShowWordInput(wordInput);
                _inputs.Add(wordInput);

                if (_service.Validate(_categoryName, wordInput))
                {
                    _score++;
                    _view.SetHealth(Math.Max(_scoreToBeat - _score, 0));
                    _view.ShowValidFeedback();
                }
                else
                {
                    _view.ShowInvalidFeedback();
                }
            }
            else
            {
                _view.ShowInvalidFeedback();
            }
        }

        public void OnTimeOut()
        {
            _view.ShowResultScreen(_score);
        }

        private void ResetData()
        {
            _inputs.Clear();
            _score = 0;
        }

        public void OnScorePopupDismissed()
        {
            _onMiniGameEnded(_score);
        }

        public void Cheat()
        {
            var words = _service.GetWords(_categoryName);
            foreach (string word in words)
            {
                OnWordInput(word);
            }
        }
    }
}