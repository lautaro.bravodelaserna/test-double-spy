using System;
using System.Collections.Generic;
using System.Linq;
using Etermax.Scripts.TuttiLadders.Services;
using Etermax.Scripts.TuttiTowers.Model;
using Etermax.Scripts.TuttiTowers.Presentation.Views;
using Etermax.Scripts.Utils;
using UniRx;

namespace Etermax.Scripts.TuttiTowers.Presentation
{
    public class GamePresenter
    {
        private readonly IGameView _view;
        private readonly IDiceService _diceService;
        private readonly Action _onPlayStarted;
        private readonly Action<int> _onFightStarted;
        private readonly Action<TurnEndData> _onTurnEnded;
        private GameData _gameData;
        private int _scoreToWin;
        protected int availableRolls;
        protected int currentPlayerPosition;
        protected CategoryCell[] _categoryCells;
        private CategoryCell _currentCategoryCell;

        public GamePresenter(IGameView view, IDiceService diceService, Action onPlayStarted, Action<int> onFightStarted, Action<TurnEndData>
        onTurnEnded)
        {
            _view = view;
            _diceService = diceService;
            _onPlayStarted = onPlayStarted;
            _onFightStarted = onFightStarted;
            _onTurnEnded = onTurnEnded;
        }

        public void StartGame(GameData gameData)
        {
            _gameData = gameData;
            _categoryCells = gameData.cells.Select(td => new CategoryCell(td)).ToArray();
            _scoreToWin = _gameData.scoreToWin;
            currentPlayerPosition = _gameData.playerPosition;
            _view.ShowBoard();
            _view.SetPlayerPosition(currentPlayerPosition);
            _view.SetOpponentPosition(_gameData.opponentPosition);
            _view.HideRollButton();
            _view.ShowStartButton();
            
            ShowCategoryCells();
            UpdateScores();
        }

        public void OnPlayEnded(int playResult)
        {
            availableRolls = playResult;
            _view.UpdatePlayerAvailableDiceRolls(availableRolls);
            _view.HideStartButton();
            if (availableRolls > 0)
                _view.ShowRollButton();
            else
                _view.HideRollButton();
            //_onTurnEnded(GetTurnEndedData());
        }

        public void RollButtonPressed()
        {
            if (availableRolls <= 0) //return;
                _onTurnEnded(GetTurnEndedData());
            
            ReduceRollCount();
            
            _view.HideRollButton();
            
            int result = _diceService.Roll();
            OnDiceRollEnded(result);
        }

        public void OnStartButtonPressed()
        {
            _onPlayStarted();
        }

        public void OnDiceRollEnded(int roll)
        {
            var previousPosition = currentPlayerPosition;

            List<IObservable<float>> playerMovements = new List<IObservable<float>>();
            
            for (int i = 0; i < roll; i++)
            {
                var nextPosition = (previousPosition + 1) % _gameData.boardSize;
                playerMovements.Add(_view.MovePlayerTowards(previousPosition, nextPosition));
                previousPosition = nextPosition;
            }

            playerMovements.Concat()
                .DoOnCompleted(() => OnPlayerEndedMoving(roll))
                .Subscribe();
        }

        protected void OnPlayerEndedMoving(int roll)
        {
            currentPlayerPosition = (currentPlayerPosition + roll) % _gameData.boardSize;
            
            var availableTower = _categoryCells.FirstOrDefault(tower => tower.GetPositions().Any(towerPosition => towerPosition == currentPlayerPosition));
            if (availableTower != null)
            {
                if (availableTower.GetOwnerId() == _gameData.playerId)
                {
                    IncreaseRollCount();
                }
                else
                {
                    _currentCategoryCell = availableTower;
                    _onFightStarted(_currentCategoryCell.GetScore());
                    _view.ShowTowerHighlight(_categoryCells.IndexOf( t => t == _currentCategoryCell));
                    return;
                }
            }

            if (IsOutOfMoves())
            {
                _onTurnEnded(GetTurnEndedData());
                return;
            }

            if (availableRolls > 0)
                _view.ShowRollButton();
            else
                _onTurnEnded(GetTurnEndedData());
        }

        private TurnEndData GetTurnEndedData()
        {
            return new TurnEndData
            {
                currentPlayerPosition = currentPlayerPosition,
                towers = GetTowersData()
            };
        }

        private CellData[] GetTowersData()
        {
            var towersData = new CellData[_gameData.cells.Length];
            for (int i = 0; i < towersData.Length; i++)
                towersData[i] = _categoryCells[i].GetData();
            return towersData;
        }

        private void ShowCategoryCells()
        {
            for (var i = 0; i < _categoryCells.Length; i++)
            {
                var cell = _categoryCells[i];
                var positions = cell.GetPositions();
                foreach (var cellPosition in positions)
                {
                    _view.ShowCategoryCellsAt(cellPosition, cell);
                }

                if (cell.GetOwnerId() == 0)
                {
                    _view.SetFlagAsDefault(i);
                }
                else if (cell.GetOwnerId() == _gameData.playerId)
                {
                    _view.SetCellOwnerAsPlayer(i);
                }
                else
                {
                    _view.SetCellOwnerAsOpponent(i);
                }

                _view.SetCellScore(i, _categoryCells[i].GetScore());
            }
        }

        public void OnFightEnded(int resultScore)
        {
            var towerIndex = _categoryCells.IndexOf( t => t == _currentCategoryCell);
            _view.HideTowerHighlight(towerIndex);
            
            if (_currentCategoryCell.GetScore() <= resultScore)
            {
                _currentCategoryCell.SetScore(resultScore);
                _currentCategoryCell.SetOwnerId(_gameData.playerId);

                _view.SetCellOwnerAsPlayer(towerIndex);

                UpdatePlayerScore();

                if (_categoryCells.Count(t => t.GetOwnerId() == _gameData.playerId) >= _scoreToWin)
                {
                    _view.ShowWinPopup();
                    return;
                }
                _view.SetCellScore(towerIndex, _currentCategoryCell.GetScore());
            }
            else
            {
                _view.SetCellScore(towerIndex, _currentCategoryCell.GetScore());
                _currentCategoryCell.SetScore(_currentCategoryCell.GetScore()-resultScore);
                _onTurnEnded(GetTurnEndedData());
                return;
            }

            if (availableRolls > 0)
                _view.ShowRollButton();
            else
            {
                _onTurnEnded(GetTurnEndedData());
            }
        }

        public void IncreaseRollCount()
        {
            availableRolls++;
            _view.UpdatePlayerAvailableDiceRolls(availableRolls);
            if (availableRolls > 0)
            {
                _view.ShowRollButton();
            }
        }

        private void ReduceRollCount()
        {
            availableRolls--;
            _view.UpdatePlayerAvailableDiceRolls(availableRolls);
        }

        private bool IsOutOfMoves()
        {
            return availableRolls <= 0;
        }

        private void UpdateScores()
        {
            var playerScore = _categoryCells.Count(t => t.GetOwnerId() == _gameData.playerId);
            var opponentScore = _categoryCells.Count(t => t.GetOwnerId() != _gameData.playerId && t.GetOwnerId() != 0);
            _view.InitializeScores(playerScore, opponentScore);
        }

        private void UpdatePlayerScore()
        {
            var playerScore = _categoryCells.Count(t => t.GetOwnerId() == _gameData.playerId);
            _view.UpdatePlayerScore(playerScore);
        }
    }
}