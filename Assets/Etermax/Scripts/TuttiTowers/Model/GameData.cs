using System.Linq;

namespace Etermax.Scripts.TuttiTowers.Model
{
    public struct GameData
    {
        public CellData[] cells;
        public int playerPosition;
        public int opponentPosition;
        public int playerId;
        public int boardSize;
        public int scoreToWin;

        public bool Equals(GameData other)
        {
            return cells.SequenceEqual(other.cells) && playerPosition == other.playerPosition && opponentPosition == other.opponentPosition &&
                   playerId == other.playerId && boardSize == other.boardSize && scoreToWin == other.scoreToWin;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is GameData && Equals((GameData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (cells != null ? cells.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ playerPosition;
                hashCode = (hashCode * 397) ^ opponentPosition;
                hashCode = (hashCode * 397) ^ playerId;
                hashCode = (hashCode * 397) ^ boardSize;
                hashCode = (hashCode * 397) ^ scoreToWin;
                return hashCode;
            }
        }
    }
}