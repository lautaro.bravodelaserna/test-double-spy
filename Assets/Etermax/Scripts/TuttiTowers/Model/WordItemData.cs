using System;

namespace Etermax.Scripts.TuttiTowers.Model
{
    [Serializable]
    public struct WordItemData
    {
        public string Word;
        public string[] Tags;

        public WordItemData(string word, string[] tags) : this()
        {
            Word = word;
            Tags = tags;
        }
    }
}