﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct MiniGameData
{
    public string character;
    public string[] categories;
    public int maxTime;
}
