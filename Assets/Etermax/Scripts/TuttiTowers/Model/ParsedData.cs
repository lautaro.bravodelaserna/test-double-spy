using UnityEngine;

namespace Etermax.Scripts.TuttiTowers.Model
{
    public class ParsedData : ScriptableObject
    {
        public WordItemData[] WordItems;
    }
}