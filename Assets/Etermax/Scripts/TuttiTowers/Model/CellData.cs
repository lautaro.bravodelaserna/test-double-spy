using System.Collections.Generic;

namespace Etermax.Scripts.TuttiTowers.Model
{
    public struct CellData
    {
        public int[] positions;
        public int score;
        public int id;

        public bool Equals(CellData other)
        {
            return score == other.score && id == other.id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is CellData && Equals((CellData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (score * 397) ^ id;
            }
        }
    }
}