using System.Collections.Generic;
using System.Linq;

namespace Etermax.Scripts.TuttiTowers.Model
{
    public struct TurnEndData
    {
        public int currentPlayerPosition;
        public CellData[] towers;
        
        public bool Equals(TurnEndData other)
        {
            return currentPlayerPosition == other.currentPlayerPosition && towers.SequenceEqual(other.towers);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is TurnEndData && Equals((TurnEndData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (currentPlayerPosition * 397) ^ (towers != null ? towers.GetHashCode() : 0);
            }
        }
    }
}