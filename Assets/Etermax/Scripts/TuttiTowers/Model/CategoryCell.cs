namespace Etermax.Scripts.TuttiTowers.Model
{
    public class CategoryCell
    {
        private readonly int[] _positions;
        private int _score;
        private int _id;

        public CategoryCell(CellData data)
        {
            _positions = data.positions;
            _score = data.score;
            _id = data.id;
        }

        public CellData GetData()
        {
            return new CellData
            {
                positions =  _positions,
                score = _score,
                id = _id
            };
        }

        public CategoryCell(int[] positions)
        {
            _positions = positions;
        }

        public int[] GetPositions()
        {
            return _positions;
        }

        public int GetScore()
        {
            return _score;
        }

        public void SetScore(int score)
        {
            _score = score;
        }

        public int GetOwnerId()
        {
            return _id;
        }

        public void SetOwnerId(int id)
        {
            _id = id;
        }
    }
}